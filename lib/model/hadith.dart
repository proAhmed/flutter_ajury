import 'package:flutter/foundation.dart';

class Hadith {
  const Hadith({
     @required this.id,
    @required this.name,
    @required this.hadithDetails,
    @required this.hadithExplain,
    @required this.hadithSource,
  })  : assert(id != null),
        assert(name != null),
        assert(hadithDetails != null),
        assert(hadithExplain != null),
      assert(hadithSource != null);
   final int id;
  final String name;
  final String hadithDetails;
  final String hadithExplain;
  final String hadithSource;

  @override
  String toString() => "$name (id=$id)";
}
