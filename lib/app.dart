import 'package:flutter/material.dart';
import 'home.dart';

class HadithApp extends StatelessWidget {
  
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: '40 Hadith',
      debugShowCheckedModeBanner:false,
      home: HomePage(),
    );
  }

}

