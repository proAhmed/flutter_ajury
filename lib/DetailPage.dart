import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hadith/model/hadith.dart';

class DetailPage extends StatefulWidget {
  final List<Hadith> hadithList;
  Hadith hadith;

  DetailPage({Key key, @required this.hadithList, @required this.hadith})
      : super(key: key);

  @override
  DetailPages createState() => DetailPages(hadithList, hadith);
}

class DetailPages extends State<DetailPage> {
  List<Hadith> hadithLst = new List();
  Hadith hadith;
  int chosenIndex = 0;
  String hadithTitle = "";
  bool showHadith = true,showExplain = true,showSource= true;

  DetailPages(List<Hadith> hadithLst, Hadith hadith) {
    this.hadithLst = hadithLst;
    this.hadith = hadith;
  }

  var appBarTitleText = new Text("");

  void setTitleBar(Hadith hadith) {
    setState(() {
      appBarTitleText = Text(hadith.name,
          style: TextStyle(color: Colors.white.withOpacity(0.8), fontSize: 20),
          textAlign: TextAlign.center);
    });
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    hadithTitle = hadith.name;
    setState(() {
      chosenIndex = hadith.id;
      hadithTitle = hadithLst[hadith.id].name;
    });
  }


  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: Text(hadithTitle,
            style:TextStyle(color: Colors.white.withOpacity(0.8), fontSize: 20),
            textAlign: TextAlign.center),
        backgroundColor: Color.fromRGBO(20, 65, 76, 100),
        centerTitle: true,
        actions: <Widget>[

          PopupMenuButton<String>(
            onSelected: (String value){
              setState(() {
              if(value=='hadith'){
                if(showHadith){
                  showHadith =false;
                }else{
                  showHadith = true;
                }
              }else if(value=="ajury"){
                if(showExplain){
                  showExplain =false;
                }else{
                  showExplain = true;
                }
              }else{
                if(showSource){
                  showSource =false;
                }else{
                  showSource = true;
                }
              }
              });
            },

            itemBuilder: (BuildContext context) =>
            <PopupMenuItem<String>>[
              const PopupMenuItem<String>(
                value: "hadith",
                child: Text('الحديث'),
              ),
              const PopupMenuItem<String>(
                value: "ajury",
                child: Text('شرح الإمام الأجرى'),
              ),
              const PopupMenuItem<String>(
                value: "source",
                child: Text('تخريج الحديث'),
              ),
            ],
          ),
        ],
      ),

      body: new Dismissible(
        resizeDuration: null,
        onDismissed: (DismissDirection direction) {
          setState(() {
            chosenIndex += direction == DismissDirection.endToStart ? 1 : -1;
            hadithTitle = hadithLst[chosenIndex].name;
          });
        },
        key: new ValueKey(chosenIndex),
        child: new ListView(
          shrinkWrap: true,
          padding: EdgeInsets.all(15.0),
          children: <Widget>[
            SizedBox(height: 20.0),
            Visibility(
              visible: showHadith,
              child:   Card(
              elevation: 3.0,
              color: Colors.white,
              child: Column(
                children: <Widget>[
                  SizedBox(height: 20.0),
                  new Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    margin: const EdgeInsets.only(left: 10.0, right: 10.0),
                    child: new Text(
                      hadithLst[chosenIndex].hadithDetails,
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 18,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  SizedBox(height: 16.0),
                ],
              ),
            ),
      ),
            Visibility(
              visible: showExplain,
              child: SizedBox(height: 30.0),
            ),
        Visibility(
          visible: showExplain,
          child:  Card(
              elevation: 4.0,
              color: Colors.white,
              child: Column(
                children: <Widget>[
                  SizedBox(height: 20.0),
                  new Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    margin: const EdgeInsets.only(left: 10.0, right: 10.0),

//                    child: RichText(
//                        text: TextSpan(
//                          text:  hadithLst[chosenIndex].hadithExplain,
//                          children: <TextSpan>[
//
//                            TextSpan(text:hadithLst[chosenIndex].hadithExplain,
//                                style:TextStyle(color: Colors.white.withOpacity(0.8), fontSize: 18) ),
//                          ],
//                        ),
//                      )
                    child: new Text(
                      hadithLst[chosenIndex].hadithExplain,
                      style: TextStyle(
                        color: Colors.black,
                        fontSize: 18,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  SizedBox(height: 16.0),
                ],
              ),
            ),
        ),
        Visibility(
          visible: showSource,
         child: SizedBox(height: 30.0),
        ),
        Visibility(
          visible: showSource, //Default is true,
          child: Card(
              elevation: 4.0,
              color: Colors.white,
              child: Column(
                children: <Widget>[
                  SizedBox(height: 20.0),
                  new Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10.0),
                    ),
                    margin: const EdgeInsets.only(left: 10.0, right: 10.0),
                    child: new Text(
                      hadithLst[chosenIndex].hadithSource,
                      style: TextStyle(
                        color: Colors.brown,
                        fontSize: 15,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  SizedBox(height: 16.0),
                ],
              ),
            ),
        ),
          ],
        ),
      ),
    );
  }
}
