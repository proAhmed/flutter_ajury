import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:hadith/DetailPage.dart';
import 'package:hadith/model/HadithRepository.dart';
import 'package:hadith/model/hadith.dart';

class HomePage extends StatelessWidget {

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(

      drawerDragStartBehavior: DragStartBehavior.down,
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text('الأربعون الأجرية'),
        backgroundColor: Color.fromRGBO(20, 65, 76, 100),
        brightness: Brightness.dark,
        centerTitle: true,
        leading: IconButton(
            icon: Icon(
              Icons.menu,
              semanticLabel: 'menu',
            ),
            onPressed: () {
              _scaffoldKey.currentState.openDrawer();
            }),
      ),


      drawer: Drawer(
        child: Column(
          children: <Widget>[
            SizedBox(height: 20.0),
            MediaQuery.removePadding(
              context: context,
              removeTop: true,
              child: Expanded(
                child: ListView(
                  dragStartBehavior: DragStartBehavior.down,
                  padding:
                      const EdgeInsets.only(top: 4.0, left: 8.0, right: 8.0),
                  children: <Widget>[
                    Stack(
                      children: <Widget>[
                        Column(
                          mainAxisSize: MainAxisSize.min,
                          children: _list().map<Widget>((Hadith hadith) {
                            return
                                Card(
                              clipBehavior: Clip.antiAlias,
                              elevation: 4.0,
                              color: Colors.white,
                              child: ListTile(
                                title: Text(hadith.name ,textAlign: TextAlign.center,style: TextStyle(fontSize: 16),),
                                onTap: () {
                                  navigate(context,_list(), hadith,);
                                },
                              ),
                            );
                          }).toList(),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),

      body: GridView.count(
        crossAxisCount: 1,
        padding: EdgeInsets.all(16.0),
        childAspectRatio: 8.0 / 2.0,
        children: _buildGridCard(context),
      ),

    );
  }
  static List<Hadith> _list() {
    List<Hadith> hadith = HadithRepository.loadHadiths();
    return hadith;
  }

  List<Card> _buildGridCard(BuildContext context) {
    List<Hadith> hadiths = HadithRepository.loadHadiths();

    if (hadiths == null || hadiths.isEmpty) {
      return const <Card>[];
    }

    return hadiths.map((hadith) {
      return Card(
        clipBehavior: Clip.antiAlias,
        elevation: 4.0,
        color: Colors.white,
        child: new InkWell(
          onTap: () {
            navigate(context, hadiths,hadith);
          },
          child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Expanded(
                child: Padding(
                  padding: EdgeInsets.fromLTRB(16.0, 30.0, 16.0, 4.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        hadith == null ? '' : hadith.name,
                        style: TextStyle(
                          color: Colors.black.withOpacity(0.8),
                          fontSize: 20,
                        ),
                        softWrap: false,
                        overflow: TextOverflow.ellipsis,
                        maxLines: 1,
                      ),
                    ],
                  ),
                ),
              )
            ],
          ),
        ),
      );
    }).toList();
  }

  void navigate(BuildContext context, List<Hadith> hadithList,Hadith hadith) {
    Navigator.push(context,
        MaterialPageRoute(builder: (context) => DetailPage(hadithList: hadithList,hadith:hadith)));
  }
}
